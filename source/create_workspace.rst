Setting up a Workspace for ROS2 & QNX
=====================================

Preferable host OS: Ubuntu 20.04

.. contents:: Table of Contents
   :depth: 2
   :local:

1- Clone the template workspace:

.. code-block:: bash

    git clone http://gitlab.com/qnx/frameworks/ros2/dev_ws.git

This workspace contains the necessary setup, toolchain file and build script to cross compile for QNX.


2- Add your packages inside dev_ws/src


3- Set the value of ROS2_HOST_INSTALLATION_PATH inside build.sh according to the location of ROS2 installation is on your pc


4- Run the build command:

.. code-block:: bash

    ./build.sh


5- On target create a new directory for your group of packages:

.. code-block:: bash

    mkdir /opt/dev_ws


6- Copy your packages over to the new location

.. code-block:: bash

    scp -r ~/dev_ws/install/x86_64/* <user_name>@<ip_address>:/opt/dev_ws


7- Add the following commands at the end of the file /etc/.profile on your target:

.. code-block:: bash

    export COLCON_CURRENT_PREFIX=/opt/ros/foxy
    . /opt/ros/foxy/local_setup.sh
    export COLCON_CURRENT_PREFIX=/opt/dev_ws
    . /opt/dev_ws/local_setup.sh   


8- Log out and log in back into new a terminal


9- Run your newly installed packages.

.. code-block:: bash

    ros2 run my_new_package my_new_package_executable
