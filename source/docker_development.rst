Building a Docker Image for ROS2 & QNX Development
================================================================================

.. contents:: Table of Contents
   :depth: 2
   :local:

Docker is a tool that can be used to easily create environments that are
reproducible and lightweight. In this tutorial we will use it to set up a
development environment for building ROS2 for QNX.

Requirements
--------------------------------------------------------------------------------

* Docker-CE

    * Can be setup on Ubuntu using Docker’s official convenience script:

.. code-block:: bash

    curl https://get.docker.com | sh && sudo systemctl --now enable docker


* QNX SDP 7.1.0

    * `Official QNX SDP 7.1.0 install instructions
      <http://www.qnx.com/developers/docs/7.1/index.html#com.qnx.doc.qnxsdp.quickstart/topic/about.html>`_

Steps
--------------------------------------------------------------------------------

1- Clone the Dockerfile.

.. code-block:: bash

    git clone https://gitlab.com/qnx/frameworks/ros2/docker

2- Prepare the docker build context with the QNX SDP. If the SDP is located in
your home directory, run the following.

.. code-block:: bash

    rsync -havz ~/qnx710 ./docker/

We use ``rsync -havz`` rather than a regular ``cp`` to preserve the symbolic
links inside the SDP. If this is not done, the size of the copied SDP will be
significantly larger.

3- Build the docker image with the included script.

.. code-block:: bash

    cd docker
    ./docker-build-qnxros2-image.sh rolling


4- Run the image with the included script to create a container. This will open
an interactive terminal into the Docker container.

.. code-block:: bash

    ./docker-create-container.sh rolling

5- Inside of the container's interactive terminal, set the CPU environment
variable to the target cpu architecture for the build of ROS2. For example:

.. code-block:: bash

    export CPU=x86_64
    # or
    export CPU=aarch64
    # or
    unset CPU # Build for all supported architectures

6- Perform the rest of the operations displayed by the welcome message to build ROS2.

.. code-block:: bash

    cd ros2_rolling
    ./build-ros2.sh
